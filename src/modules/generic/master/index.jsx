import React from 'react'

function Master(props) {
    return (
        <div>
            I am master page
            {props.children}
        </div>

    )
}

export default Master