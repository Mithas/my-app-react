import React, { useState, useEffect } from "react";

import useStyles from "./loginStyle";

function Login() {
  const [form, setForm] = useState({
    email: "",
    password: ""
  });

  useEffect(() => {
    console.log(form);
  }, [form.password]);

  const handleChange = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = e => {
    console.log("form submitted");
  };
  const classes = useStyles();
  return (
    <>
      <form
        className={classes.form}
        title={"Login Page"}
        onSubmit={handleSubmit}
      >
        <label className={classes.gridLeft}>Name:</label>
        <input
          type="text"
          name="email"
          value={form.email}
          className={classes.gridRight}
          onChange={handleChange}
        ></input>

        <label className={classes.gridLeft}>Password:</label>
        <input
          type="password"
          name="password"
          className={classes.gridRight}
          value={form.password}
          onChange={handleChange}
        ></input>

        <button className={classes.myButton} type="submit">
          Submit
        </button>
      </form>
    </>
  );
}

export default Login;
