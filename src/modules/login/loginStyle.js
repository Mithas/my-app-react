import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  form: {
    display: "grid",
    gridTemplateColumns: "1fr 3fr",
    gridGap: "1em",
    width: "50%",
    gridAutoRows: "minMax(32px,auto)",
    padding: "1em",
    background: "indianred"
  },
  myButton: {
    width: "fit-content",
    gridColumn: "2"
  }
});

export default useStyles;
