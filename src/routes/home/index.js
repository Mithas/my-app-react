
import { lazy, } from 'react'

const LazyHome = lazy(() => import('../../modules/home'))

export default LazyHome