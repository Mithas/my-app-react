import { BrowserRouter as Router, Route, Switch, } from 'react-router-dom'
import React, { Suspense, } from 'react'

import Home from './home'
import Login from './login'
import Master from '../modules/generic/master'

const RootRouter = () => (
    <Router>
        <Suspense fallback={<div>Loading...</div>}>
            <Switch>
                <Route exact path="/home" component={Home} />
                <Master>
                    <Route exact path="/login" component={Login} />
                </Master>
            </Switch>
        </Suspense>
    </Router>
)

export default RootRouter

