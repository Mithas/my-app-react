
import { lazy, } from 'react'

const LazyLogin = lazy(() => import('../../modules/login'))

export default LazyLogin